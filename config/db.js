import mysql from 'mysql2/promise';

const host = process.env.NEXT_PUBLIC_DATABASE_HOST;
const port = process.env.NEXT_PUBLIC_DATABASE_PORT;
const user = process.env.NEXT_PUBLIC_DATABASE_USER;
const password = process.env.NEXT_PUBLIC_DATABASE_PASSWORD;
const database = process.env.NEXT_PUBLIC_DATABASE_NAME;
const db = mysql.createPool({
  host: process.env.NEXT_PUBLIC_DATABASE_HOST,
  port: process.env.NEXT_PUBLIC_DATABASE_PORT,
  user: process.env.NEXT_PUBLIC_DATABASE_USER,
  password: process.env.NEXT_PUBLIC_DATABASE_PASSWORD,
  database: process.env.NEXT_PUBLIC_DATABASE_NAME,
});
console.log(host, port, user, password, database)
export default db;