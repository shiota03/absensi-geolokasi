/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect } from "react";

import HeaderAdminPanel from "@/components/admin-panel/Header";
import SidebarAdminPanel from "@/components/admin-panel/Sidebar";
import PageHeaderAdmin from "@/components/admin-panel/PageHeader";
import FormTambahData from "@/components/admin-panel/data-pegawai/FormTambahData";

export default function tambahDataAdmin(){
    useEffect(() => {
        document.getElementsByClassName('menu-side')[2].classList.add('active')
    }, [])
    return(
        <>
            <HeaderAdminPanel />
            <SidebarAdminPanel />
            <section className="relative" id="content">
                <PageHeaderAdmin text={'Admin / Data Admin / Tambah Data'} />
                <div>
                    <FormTambahData role={0} />
                </div>
            </section>
        </>
    )
}