/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";

import HeaderAdminPanel from "@/components/admin-panel/Header";
import SidebarAdminPanel from "@/components/admin-panel/Sidebar";
import PageHeaderAdmin from "@/components/admin-panel/PageHeader";
import axios from "axios";
import { useRouter } from "next/router";
import FormEditData from "@/components/admin-panel/data-pegawai/FormEditData";

export default function EditDataAdmin (){
    const router = useRouter();
    const { id } = router.query;

    useEffect(() => {
        document.getElementsByClassName('menu-side')[2].classList.add('active')
    }, [])
    
    const [dataPegawai, setDataPegawai] = useState({
        nama: '',
        id: ''
    });
    const apiENV = process.env.NEXT_PUBLIC_API_KEY;

    const userShow = (id: any) => {    
        axios.get(`${apiENV}/showUser?id=${id}`)
        .then((response) => {
            let dataKaryawan = response.data.data;
            setDataPegawai({
                nama: dataKaryawan.nama,
                id: dataKaryawan.id
            })
        })
        .catch((error) => {
            console.error(error)
        })
    }

    useEffect(() => {
        userShow(id)
    }, [id])
    return(
        <>
            <HeaderAdminPanel />
            <SidebarAdminPanel />
            <section className="relative" id="content">
                <PageHeaderAdmin text={`Admin / Data Admin / Edit / ${dataPegawai.nama}`} />
                <FormEditData userId={dataPegawai.id} role={0} />
            </section>
        </>
    )
}