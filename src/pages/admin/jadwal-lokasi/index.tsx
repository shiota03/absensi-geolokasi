import HeaderAdminPanel from "@/components/admin-panel/Header";
import PageHeaderAdmin from "@/components/admin-panel/PageHeader";
import SidebarAdminPanel from "@/components/admin-panel/Sidebar";
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';

import { useEffect } from "react";
import JamKerja from "@/components/admin-panel/jadwal-lokasi/JamKerja";
import dynamic from 'next/dynamic'

const Map = dynamic(() => import('@/components/admin-panel/jadwal-lokasi/Maps'), {
  ssr: false
})

export default function JadwalLokasiAdmin() {
  useEffect(() => {
    document.getElementsByClassName('menu-side')[1].classList.add('active')
  }, [])
  return(
    <>
      <HeaderAdminPanel />
      <SidebarAdminPanel />
      <section className="relative" id="content">
        <PageHeaderAdmin text={`Admin / Jadwal dan Lokasi`} />
        <div className="grid lg:grid-cols-5 md:grid-cols-7 gap-8">
          <JamKerja />
          <Map />
        </div>
      </section>
    </>
  )
}
