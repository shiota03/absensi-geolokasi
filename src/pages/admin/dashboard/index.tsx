import HeaderAdminPanel from "@/components/admin-panel/Header";
import { useState, useEffect } from 'react';
import axios from 'axios';
import SidebarAdminPanel from "@/components/admin-panel/Sidebar";
import PageHeaderAdmin from "@/components/admin-panel/PageHeader";

export default function DashboardAdmin(){
    const [nama, setNama] = useState('');
    const getUserDataById = async(id: any) => {
        axios.get(`http://localhost:3000/api/loginApi?id=${id}`)
        .then((response) => {
            let userData = response.data.data;
            setNama(userData.nama);
        })
        .catch((error) => {
            console.error(error)
        })
    }
    useEffect(() => {
        const userId = localStorage.getItem("user_id");
        document.getElementsByClassName('menu-side')[0].classList.add('active')
        getUserDataById(userId);
    }, [])

    return(
        <>
            <HeaderAdminPanel />
            <SidebarAdminPanel />
            <section className="relative" id="content">
                <PageHeaderAdmin text={'Admin / Dashboard'} />
            </section>
        </>
    )
}