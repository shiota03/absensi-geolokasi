import HeaderAdminPanel from "@/components/admin-panel/Header";
import PageHeaderAdmin from "@/components/admin-panel/PageHeader";
import SidebarAdminPanel from "@/components/admin-panel/Sidebar";
import { faDownload, faEdit, faSquarePlus, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Link from "next/link";
import { useEffect, useState } from "react";
import AdminPageDatatable from "@/components/datatable/admin/AdminPageDatatable";

export default function DataPegawaiAdmin(){
    useEffect(() => {
        document.getElementsByClassName('menu-side')[3].classList.add('active')
    }, [])

    
    return(
        <>
            <HeaderAdminPanel />
            <SidebarAdminPanel />
            <section className="relative" id="content">
                <PageHeaderAdmin text={'Admin / Data Pegawai'} />
                <div className="md:flex md:justify-end text-right">
                    <Link href={"/admin/data-pegawai/tambah-data"} className="md:text-[14px] text-[12px] bg-blue-600 py-2 px-3 rounded-[7px] text-white mb-1 md:me-2">
                        <FontAwesomeIcon className="me-1" icon={faSquarePlus} /> Tambah Data Pegawai
                    </Link>
                    <button className="md:text-[14px] text-[12px] bg-green-600 py-2 px-3 rounded-[7px] text-white mb-1 md:mt-0 mt-3">
                        <FontAwesomeIcon className="me-1" icon={faDownload} /> Export Data Pegawai
                    </button>
                </div>
                <div className="w-[100%] card">
                    <AdminPageDatatable role={1} />
                </div>
            </section>
        </>
    )
}