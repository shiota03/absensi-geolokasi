/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect } from "react";

import HeaderAdminPanel from "@/components/admin-panel/Header";
import SidebarAdminPanel from "@/components/admin-panel/Sidebar";
import PageHeaderAdmin from "@/components/admin-panel/PageHeader";
import FormTambahData from "@/components/admin-panel/data-pegawai/FormTambahData";

export default function tambahDataPegawai(){
    useEffect(() => {
        document.getElementsByClassName('menu-side')[3].classList.add('active')
    }, [])
    return(
        <>
            <HeaderAdminPanel />
            <SidebarAdminPanel />
            <section className="relative" id="content">
                <PageHeaderAdmin text={'Admin / Data Pegawai / Tambah Data'} />
                <div>
                    <FormTambahData role={1} />
                </div>
            </section>
        </>
    )
}