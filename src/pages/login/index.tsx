import Header from "@/components/landing-page/auth/Header";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import axios from "axios";
import Swal from "sweetalert2";

export default function Login(){
    const apiKey = process.env.NEXT_PUBLIC_API_KEY;

    const [formLogin, setFormLogin] = useState({
        identitas: '',
        password: ''
    });
    const [errors, setErrors] = useState({
        identitas: '',
        password: '',
    });
    const styleBackground = {
        backgroundImage: 'linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url("/assets/images/background-firstpage.jpg")',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundAttachment: 'fixed'
    }
    const handleChangeLogin = (e: { target: { name: any; value: any; }; }) => {
        const { name, value } = e.target;
        setFormLogin({
            ...formLogin,
            [name]: value
        })
    }
    const handleLoginSubmit = async(e: { preventDefault: () => void; }) => {
        e.preventDefault();
        let isValid = true;
        const newErrors = { ...errors };
        if (!formLogin.identitas.trim()) { newErrors.identitas = 'Nomor Identitas harus diisi'; isValid = false; } else { newErrors.identitas = ''; }
        if (!formLogin.password.trim()) { newErrors.password = 'Password harus diisi'; isValid = false; } else { newErrors.password = ''; }
        setErrors(newErrors);
        if (isValid) {
            const dataLogin  = {
                identitas: formLogin.identitas,
                password: formLogin.password
            }
            axios.post(`${apiKey}/loginApi`, dataLogin)
            .then((response) => {
                if(response.data.code === 401){
                    Swal.fire({
                        icon: 'error',
                        title: 'Login gagal',
                        text: response.data.message
                    })
                } else {
                    Swal.fire({
                        icon: 'success',
                        title: 'Login berhasil'
                    }).then(() => {
                        localStorage.setItem("user_id", response.data.data.id);
                        localStorage.setItem("user_role", response.data.data.role);
                        if(response.data.data.role === 0){
                            window.location.href = '/admin/dashboard'
                        } else {
                            window.location.href = '/user/dashboard'
                        }
                    })
                }
            })
            .catch((error) => {
                
            })
        }
    }
    return(
        <div>
            <Header />
            <section className='absolute top-0 h-[100vh] w-[100%] z-[-10]' style={styleBackground}>
                <div className='flex justify-center items-center h-[100vh]'>
                    <div className='bg-white rounded-[10px] px-4 py-5 lg:w-1/2 text-center'>
                        <div className="flex items-center lg:mx-10">
                            <div className="basis-1/2">
                                <Link href="/">
                                    <Image width={50} height={50} src="https://flowbite.com/docs/images/logo.svg" className="" alt="Flowbite Logo" />
                                </Link>
                            </div>
                            <h1 className='text-[2rem] text-right basis-1/2 font-serif'>
                                <strong>
                                    LOGIN
                                </strong>
                            </h1>
                        </div>
                        <hr className="border-black my-2" />
                        <form className='text-left' onSubmit={handleLoginSubmit}>
                            <div>
                                <label htmlFor="identitas" className="font-serif">
                                    <strong>Nomor Identitas <em className="text-red-500">*</em></strong>
                                </label>
                                <br />
                                <input type="number" name="identitas" onChange={handleChangeLogin} value={formLogin.identitas} id="identitas" className="border-y border-x border-gray-500 px-3 py-2 rounded-lg bg-transparent w-[100%] outline-none mt-1 font-mono" />
                                <div className='text-red-500 text-sm font-semibold'><em>{errors.identitas}</em></div>
                            </div>
                            <div>
                                <label htmlFor="password" className="font-serif">
                                    <strong>Password <em className="text-red-500">*</em></strong>
                                </label>
                                <br />
                                <input type="password" name="password" id="password" onChange={handleChangeLogin} value={formLogin.password} className="border-y border-x border-gray-500 px-3 py-2 rounded-lg bg-transparent w-[100%] outline-none mt-1 font-mono" />
                                <div className='text-red-500 text-sm font-semibold'><em>{errors.password}</em></div>
                            </div>
                            <div>
                                <button className="w-[100%] mt-2 py-2 bg-blue-800 hover:bg-slate-400 duration-300 text-white text-base font-bold rounded-lg">
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    )
}