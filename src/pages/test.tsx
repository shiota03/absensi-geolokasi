import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import dynamic from 'next/dynamic'

const Map = dynamic(() => import('@/components/admin-panel/jadwal-lokasi/Maps'), {
  ssr: false
})
export default function ForTest(){
    return(
        <>
            <Map />
        </>
    )
}