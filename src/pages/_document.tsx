/* eslint-disable @next/next/no-css-tags */
import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="en">
      <title>
        Zero Next.Js
      </title>
      <link rel="stylesheet" href="/assets/css/admin-layout.css" />
      <link rel="stylesheet" href="/assets/css/datatable.css" />
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
