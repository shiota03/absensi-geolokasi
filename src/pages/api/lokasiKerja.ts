/* eslint-disable import/no-anonymous-default-export */
import { NextApiRequest, NextApiResponse } from 'next';
import db from '../../../config/db';
const bcrypt = require('bcrypt');

interface LokasiKerja {
    lat: string;
    lng: string;
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const { method, query } = req;
    switch (method) {
        case 'GET':
            const [queryResult] = await db.query('SELECT * FROM lokasi_kerja WHERE id = 1');
            const data: LokasiKerja[] = queryResult as LokasiKerja[];
            res.status(200).json({
                data: data[0],
                message: 'Data jam kerja ditemukan',
                code: 200,
                status: 'Success'
            })
            break;
        case 'PUT':
            const {lat, lng} = req.body || query
            try {
                await db.query("update lokasi_kerja set lat = ? , lng = ? where id = 1 ", [lat, lng]);
                res.status(200).json({
                    message: 'Data berhasil diupdate',
                    code: 200,
                    status: 'success'
                });
                return;
            } catch (error) {
                res.status(500).json({ error: 'Internal server error' });
            }
            break;
        default:
            res.status(405).end();
        break;
  }
}