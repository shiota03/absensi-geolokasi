/* eslint-disable import/no-anonymous-default-export */
import { NextApiRequest, NextApiResponse } from 'next';
import db from '../../../config/db';
import { useState } from 'react';
const bcrypt = require('bcrypt');

interface UserData {
    id: number;
    nomor_identitas: string;
    email: string;
    nomor_hp: string;
    password: string;
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const { method, query } = req;
    switch (method) {
        case 'GET':
            if(!query.id){
                const role = query.role
                const [data] = await db.query('SELECT * FROM users WHERE role = ?', [role]);
                res.status(200).json({
                    data: data,
                    message: 'Data user ditemukan',
                    code: 200,
                    status: 'Success'
                })
            }
            if(!query.role){
                const id = query.id
                const [queryResult] = await db.query('SELECT * FROM users WHERE id = ?', [id]);
                const data: UserData[] = queryResult as UserData[];
                res.status(200).json({
                    data: data[0],
                    message: 'Data user ditemukan',
                    code: 200,
                    status: 'Success'
                })
            }
            break;
        case 'POST':
            const {nama, nomor_identitas, password, email, nomor_hp, jabatan, alamat, jenis_kelamin, tanggal_masuk_kerja} = req.body || query
            try {
                const [queryResultCekIdentitas] = await db.query('SELECT * FROM users WHERE nomor_identitas = ?', [nomor_identitas]);
                const cekIdentitas: UserData[] = queryResultCekIdentitas as UserData[];

                const [queryResultCekEmail] = await db.query('SELECT * FROM users WHERE email = ?', [email]);
                const cekEmail: UserData[] = queryResultCekEmail as UserData[];

                const [queryResultCekNomorHp] = await db.query('SELECT * FROM users WHERE nomor_hp = ?', [nomor_hp]);
                const cekNomorHp: UserData[] = queryResultCekNomorHp as UserData[];

                if(cekIdentitas.length > 0){
                    res.status(200).json({
                        message: 'Nomor identitas sudah pernah digunakan',
                        code: 401,
                        status: 'Error'
                    });
                    return;
                }
                if(cekEmail.length > 0){
                    res.status(200).json({
                        message: 'Email sudah pernah digunakan',
                        code: 401,
                        status: 'Error'
                    });
                    return;
                }
                if(cekNomorHp.length > 0){
                    res.status(200).json({
                        message: 'Nomor Hp sudah pernah digunakan',
                        code: 401,
                        status: 'Error'
                    });
                    return;
                }
                const hashedPassword = await bcrypt.hash(password, 10);
                await db.query(''+
                    'INSERT INTO users '+
                    '(id, nama, nomor_identitas, password, email, nomor_hp, jabatan, alamat, jenis_kelamin, mulai_bekerja, role)  '+
                    'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                    [null, nama, nomor_identitas, hashedPassword, email, nomor_hp, jabatan, alamat, jenis_kelamin, tanggal_masuk_kerja, query.role]
                );
                res.status(201).json({
                    message: 'Data berhasil ditambahkan',
                    code: 201,
                    status: 'Success'
                });
                return;
            } catch (error) {
                res.status(500).json({ error: 'Internal server error' });
            }
            break;
        case 'DELETE':
            const userId = query.id;
            if (!userId) {
                res.status(200).json({
                    message: 'ID pengguna tidak ditemukan',
                    code: 400,
                    status: 'Error',
                });
                return;
            }
            try {
                await db.query('DELETE FROM users WHERE id = ?', [userId]);
                res.status(200).json({
                    message: 'Data berhasil dihapus',
                    code: 200,
                    status: 'Success',
                });
                return;
            } catch (error) {
                res.status(500).json({ error: 'Internal server error' });
            }
            break;
        case 'PUT':
            const {id_update, nama_update, nomor_identitas_update, password_update, email_update, nomor_hp_update, jabatan_update, alamat_update, jenis_kelamin_update, tanggal_masuk_kerja_update} = req.body || query
            try {
                const [queryResultId] = await db.query('SELECT * FROM users WHERE id = ?', [id_update]);
                const dataId: UserData[] = queryResultId as UserData[];

                if(dataId[0].nomor_identitas != nomor_identitas_update){
                    const [queryResultCekIdentitas] = await db.query('SELECT * FROM users WHERE nomor_identitas = ?', [nomor_identitas]);
                    const cekIdentitas: UserData[] = queryResultCekIdentitas as UserData[];
                    if(cekIdentitas.length > 0){
                        res.status(200).json({
                            message: 'Nomor identitas sudah pernah digunakan',
                            code: 401,
                            status: 'Error'
                        });
                        return;
                    }
                }
                if(dataId[0].email != email_update){
                    const [queryResultCekEmail] = await db.query('SELECT * FROM users WHERE email = ?', [email]);
                    const cekEmail: UserData[] = queryResultCekEmail as UserData[];
                    if(cekEmail.length > 0){
                        res.status(200).json({
                            message: 'Email sudah pernah digunakan',
                            code: 401,
                            status: 'Error'
                        });
                        return;
                    }
                }
                if(dataId[0].nomor_hp != nomor_hp_update){
                    const [queryResultCekNomorHp] = await db.query('SELECT * FROM users WHERE nomor_hp = ?', [nomor_hp]);
                    const cekNomorHp: UserData[] = queryResultCekNomorHp as UserData[];
                    if(cekNomorHp.length > 0){
                        res.status(200).json({
                            message: 'Nomor hp sudah pernah digunakan',
                            code: 401,
                            status: 'Error'
                        });
                        return;
                    }
                }
                const hashedPassword = dataId[0].password === password_update ? password_update : await bcrypt.hash(password_update, 10);
                const dataUpdateSend = [
                    nama_update, 
                    nomor_identitas_update, 
                    hashedPassword, 
                    email_update, 
                    nomor_hp_update, 
                    jabatan_update, 
                    alamat_update, 
                    jenis_kelamin_update, 
                    tanggal_masuk_kerja_update,
                    id_update
                ];
                await db.query(''+
                    'update users set '+
                    'nama = ? , '+
                    'nomor_identitas = ? , '+
                    'password = ? , '+
                    'email = ? , '+
                    'nomor_hp = ? , '+
                    'jabatan = ? , '+
                    'alamat = ? , '+
                    'jenis_kelamin = ? , '+
                    'mulai_bekerja = ? '+
                    'where id = ? ', 
                    dataUpdateSend
                );

                res.status(200).json({
                    message: 'Data berhasil diupdate',
                    code: 200,
                    status: 'success'
                });
                return;
            } catch (error) {
                res.status(500).json({ error: 'Internal server error' });
            }
            break;
        default:
            res.status(405).end();
        break;
  }
}