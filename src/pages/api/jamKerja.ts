/* eslint-disable import/no-anonymous-default-export */
import { NextApiRequest, NextApiResponse } from 'next';
import db from '../../../config/db';
const bcrypt = require('bcrypt');

interface JamKerja {
    jam_status: string;
    dari: string;
    sampai: string;
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const { method, query } = req;
    switch (method) {
        case 'GET':
            if(!query.status){
                const [queryResult] = await db.query('SELECT * FROM jam_kerja');
                const data: JamKerja[] = queryResult as JamKerja[];
                res.status(200).json({
                    data: data,
                    message: 'Data jam kerja ditemukan',
                    code: 200,
                    status: 'Success'
                })
            }
            const status = query.status
            const [queryResult] = await db.query('SELECT * FROM jam_kerja WHERE jam_status = ?', [status]);
            const data: JamKerja[] = queryResult as JamKerja[];
            res.status(200).json({
                data: data[0],
                message: 'Data jam kerja ditemukan',
                code: 200,
                status: 'Success'
            })
            break;
        case 'PUT':
            const {dari_jam_masuk, sampai_jam_masuk, dari_jam_pulang} = req.body || query
            try {
                if(query.status === 'Masuk'){
                    const data = [ dari_jam_masuk, sampai_jam_masuk, query.status];
                    await db.query('update jam_kerja set dari = ? ,sampai = ? where jam_status = ? ', data);
                    res.status(200).json({
                        message: 'Data berhasil diupdate',
                        code: 200,
                        status: 'success'
                    });
                } else if(query.status === 'Pulang'){
                    const data = [ dari_jam_pulang, query.status];
                    await db.query('update jam_kerja set dari = ? where jam_status = ? ', data);
                    res.status(200).json({
                        message: 'Data berhasil diupdate',
                        code: 200,
                        status: 'success'
                    });
                }

                res.status(200).json({
                    message: 'Data gagal diupdate',
                    code: 401,
                    status: 'Error'
                });
                return;
            } catch (error) {
                res.status(500).json({ error: 'Internal server error' });
            }
            break;
        default:
            res.status(405).end();
        break;
  }
}