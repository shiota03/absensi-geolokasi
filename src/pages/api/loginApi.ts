/* eslint-disable import/no-anonymous-default-export */
import { NextApiRequest, NextApiResponse } from 'next';
import db from '../../../config/db';
const bcrypt = require('bcrypt');

interface User {
    id: number;
    password: string;
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const { method, query } = req;
    switch (method) {
        case 'GET':
            const id = query.id
            const [queryResult] = await db.query('SELECT * FROM users WHERE id = ?', [id]);
            const data: User[] = queryResult as User[];
            res.status(200).json({
                data: data[0],
                message: 'Data user ditemukan',
                code: 200,
                status: 'Sukses'
            })
            break;
        case 'POST':
            const {identitas, password} = req.body || query
            try {
                const [queryResult] = await db.query('SELECT * FROM users WHERE nomor_identitas = ?', [identitas]);
                const cekIdentitas: User[] = queryResult as User[];
                if(cekIdentitas.length === 0){
                    res.status(200).json({
                        message: 'Nomor identitas tidak terdaftar',
                        code: 401,
                        status: 'Error'
                    });
                    return;
                }
                const data = cekIdentitas[0];
                const passwordMatched = await bcrypt.compare(password, data.password);
                if(!passwordMatched){
                    res.status(200).json({
                        message: 'Data login tidak sesuai',
                        code: 401,
                        status: 'Error'
                    });
                    return;
                }
                res.status(200).json({
                    data: data,
                    message: 'Data login tidak sesuai',
                    code: 200,
                    status: 'Error'
                })
            } catch (error) {
                res.status(500).json({ error: 'Internal server error' });
            }
            break;
        default:
            res.status(405).end();
        break;
  }
}