/* eslint-disable import/no-anonymous-default-export */
import { NextApiRequest, NextApiResponse } from 'next';
import db from '../../../config/db';
const bcrypt = require('bcrypt');

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const { method, query } = req;
    switch (method) {
        case 'POST':
            try {
                const {identitas, password, role = 1} = query
                if (!identitas || !password) {
                    res.status(401).json({
                        message: 'Data tidak lengkap',
                        code: 401,
                        status: 'Error'
                    });
                    return;
                }
                const hashedPassword = await bcrypt.hash(password, 10);
                const cekIdentitas = await db.query('SELECT * FROM users WHERE nomor_identitas = ?', [identitas]);
                if(cekIdentitas){
                    res.status(401).json({
                        message: 'Nomor identitas sudah pernah digunakan',
                        code: 401,
                        status: 'Error'
                    });
                    return;
                }
                await db.query('INSERT INTO users (id, nomor_identitas, password, role) VALUES (?, ?, ?, ?)', [null, identitas, hashedPassword, 1]);
                res.status(201).json(
                    {
                        message: 'Data berhasil ditambahkan',
                        code: 201,
                        status: 'Success'
                    }
                );
            } catch (error) {
                res.status(500).json({ error: 'Internal server error' });
            }
            break;
        default:
            res.status(405).end();
        break;
  }
}