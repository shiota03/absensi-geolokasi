import { useState } from "react";

import axios from "axios";
import Swal from "sweetalert2";

const FormTambahData = ({ role = 0 }) => {
    const apiENV = process.env.NEXT_PUBLIC_API_KEY;
    const [userForm, setUserForm] = useState({
        nama: '',
        nomor_identitas: '',
        password: '123456',
        email: '',
        nomor_hp: '',
        jabatan: '',
        alamat: '',
        jenis_kelamin: '',
        tanggal_masuk_kerja: ''
    })

    const handleInputChange = (e: { target: { name: any; value: any; }; }) => {
        const { name, value } = e.target;
        setUserForm((prevData: any) => ({
          ...prevData,
          [name]: value,
        }));
    };
    const submitPegawai = async(e: { preventDefault: () => void; }) => {
        e.preventDefault();
        const dataSubmit = {
            nama: userForm.nama,
            nomor_identitas: userForm.nomor_identitas,
            password: userForm.password,
            email: userForm.email,
            nomor_hp: userForm.nomor_hp,
            jabatan: userForm.jabatan,
            alamat: userForm.alamat,
            jenis_kelamin: userForm.jenis_kelamin || 'Laki - Laki',
            tanggal_masuk_kerja: userForm.tanggal_masuk_kerja
        };
        axios.post(`${apiENV}/showUser?role=${role}`, dataSubmit)
        .then((response) => {
            if(response.data.code === 401){
                Swal.fire({
                    icon: 'error',
                    title: 'Login gagal',
                    text: response.data.message
                })
            } else {
                Swal.fire({
                    icon: 'success',
                    title: response.data.message
                }).then(() => {
                    if(role > 0){
                        window.location.href = '/admin/data-pegawai'
                    } else {
                        window.location.href = '/admin/data-admin'
                    }
                })
            }
        })
        .catch((error) => {
            console.error(error)
        })
    }
    return(
        <>
            <form onSubmit={submitPegawai}>
                <div className="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1 md:gap-3 gap-1">
                    <div>
                        <label htmlFor="nama"><strong>Nama</strong></label><br />
                        <input onChange={handleInputChange} name="nama" type="text" id="nama" className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" required />
                    </div>
                    <div>
                        <label htmlFor="nomor_identitas"><strong>Nomor Identitas</strong></label><br />
                        <input onChange={handleInputChange} name="nomor_identitas" type="number" id="nomor_identitas" className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" required />
                    </div>
                    <div>
                        <label htmlFor="password"><strong>Password (default : 123456)</strong></label><br />
                        <input onChange={handleInputChange} name="password" type="password" id="password" className="mt-1 bg-gray-200 border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" required value={'123456'} readOnly />
                    </div>
                    <div>
                        <label htmlFor="email"><strong>Email</strong></label><br />
                        <input onChange={handleInputChange} name="email" type="email" id="email" className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" required />
                    </div>
                    <div>
                        <label htmlFor="nomor_hp"><strong>Nomor Hp</strong></label><br />
                        <input onChange={handleInputChange} name="nomor_hp" type="number" id="nomor_hp" className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" required />
                    </div>
                    <div>
                        <label htmlFor="jabatan"><strong>Jabatan</strong></label><br />
                        <input onChange={handleInputChange} name="jabatan" type="text" id="jabatan" className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" required />
                    </div>
                    <div className="row-span-2">
                        <div>
                            <label htmlFor="alamat"><strong>Alamat</strong></label><br />
                            <textarea onChange={handleInputChange} id="alamat" name="alamat" className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 lg:h-[100px] md:h-[120px] px-2"></textarea>
                        </div>
                    </div>
                    <div>
                        <label htmlFor="jenis_kelamin"><strong>Jenis Kelamin</strong></label><br />
                        <select onChange={handleInputChange} name="jenis_kelamin" id="jenis_kelamin" className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2">
                            <option value="Laki - Laki">Laki - Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div>
                        <label htmlFor="tanggal_masuk_kerja"><strong>Tanggal Masuk Kerja</strong></label><br />
                        <input onChange={handleInputChange} name="tanggal_masuk_kerja" type="date" id="tanggal_masuk_kerja" className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" required />
                    </div>
                    <div className="md:col-span-2">
                        <button className="w-[100%] bg-blue-500 rounded-[7px] py-2 text-white font-bold md:mt-0 mt-2">Simpan</button>
                    </div>
                </div>
            </form>
        </>
    )
}
export default FormTambahData;