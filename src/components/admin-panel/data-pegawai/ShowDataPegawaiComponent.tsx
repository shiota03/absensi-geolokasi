/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";

import axios from "axios";
import { useQRCode } from "next-qrcode";
import Link from "next/link";
const ShowDataPegawaiComponent = ({ role = 0, userId = '' }) => {    
    const [dataPegawai, setDataPegawai] = useState({
        id: '',
        nama: '',
        nomor_identitas: '',
        email: '',
        nomor_hp: '',
        jabatan: '',
        alamat: '',
        jenis_kelamin: ''

    });
    const [qrCode, setQrCode] = useState();
    const apiENV = process.env.NEXT_PUBLIC_API_KEY;

    const [tanggal, setTanggal] = useState('');
    const [bulan, setBulan] = useState('')
    const [tahun, setTahun] = useState('')

    const userShow = (id: any) => {    
        axios.get(`${apiENV}/showUser?id=${id}`)
        .then((response) => {
            let dataKaryawan = response.data.data;
            setDataPegawai({
                id: dataKaryawan.id,
                nama: dataKaryawan.nama,
                nomor_identitas: dataKaryawan.nomor_identitas,
                email: dataKaryawan.email,
                nomor_hp: dataKaryawan.nomor_hp,
                jabatan: dataKaryawan.jabatan,
                alamat: dataKaryawan.alamat,
                jenis_kelamin: dataKaryawan.jenis_kelamin
            });
            setQrCode(dataKaryawan.nomor_identitas);
            const startDate = new Date(dataKaryawan.mulai_bekerja);
            setTahun(startDate.getFullYear().toString());
            setTanggal(startDate.getDate().toString());
            setBulan((startDate.getMonth() + 1).toString());
        })
        .catch((error) => {
            console.error(error)
        })
    }

    useEffect(() => {
        userShow(userId)
    }, [userId])


    const { SVG } = useQRCode();

    return(
        <>
            <div>
                <div className="lg:grid lg:grid-cols-5 md:gap-6">
                    <div className="lg:col-span-2 lg:mb-0 md:mb-3 mb-4">
                    {qrCode && (
                        <div className="lg:col-span-2 lg:mb-0 md:mb-3 mb-4">
                            <div className="border lg:w-[80%] lg:ml-[10%] md:w-[30%] md:ml-[35%] w-[100%] ">
                            <SVG
                                text={qrCode}
                                options={{
                                margin: 2,
                                color: {
                                    dark: '#000',
                                }
                                }}
                            />
                            </div>
                        </div>
                    )}
                    </div>
                    <div className="lg:col-span-3">
                        <table className="table-auto w-[100%]">
                            <tbody>
                                <tr>
                                    <th className="text-left border border-t-0 border-x-0 border-gray-500 p-1">Nama</th>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">:</td>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">{ dataPegawai.nama }</td>
                                </tr>
                                <tr>
                                    <th className="text-left border border-t-0 border-x-0 border-gray-500 p-1">Nomor Identitas</th>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">:</td>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">{ dataPegawai.nomor_identitas }</td>
                                </tr>
                                <tr>
                                    <th className="text-left border border-t-0 border-x-0 border-gray-500 p-1">Email</th>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">:</td>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">{ dataPegawai.email }</td>
                                </tr>
                                <tr>
                                    <th className="text-left border border-t-0 border-x-0 border-gray-500 p-1">Nomor Hp</th>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">:</td>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">{ dataPegawai.nomor_hp }</td>
                                </tr>
                                <tr>
                                    <th className="text-left border border-t-0 border-x-0 border-gray-500 p-1">Jabatan</th>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">:</td>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">{ dataPegawai.jabatan }</td>
                                </tr>
                                <tr>
                                    <th className="text-left border border-t-0 border-x-0 border-gray-500 p-1">Jenis Kelamin</th>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">:</td>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">{ dataPegawai.jenis_kelamin }</td>
                                </tr>
                                <tr>
                                    <th className="text-left border border-t-0 border-x-0 border-gray-500 p-1">Alamat</th>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">:</td>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">{ dataPegawai.alamat }</td>
                                </tr>
                                <tr>
                                    <th className="text-left border border-t-0 border-x-0 border-gray-500 p-1">Tanggal Mulai Bekerja</th>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">:</td>
                                    <td className="border border-t-0 border-x-0 border-gray-500 p-1">{ tanggal }-{ bulan }-{ tahun }</td>
                                </tr>
                                <tr>
                                    <th colSpan={3} className="border border-t-0 border-x-0 border-gray-500 p-1">
                                        <div className="my-4 grid grid-cols-2">
                                        <div>
                                                <Link href={'#'} className="bg-green-500 w-[100%] py-2 px-3 rounded text-white">Download Profil</Link>
                                            </div>
                                            <div>
                                                {role > 0 ? (
                                                    <Link href={`/admin/data-pegawai/edit/${dataPegawai.id}`} className="bg-yellow-500 w-[100%] py-2 px-3 rounded text-white">Edit Data</Link>
                                                ) : (
                                                    <Link href={`/admin/data-admin/edit/${dataPegawai.id}`} className="bg-yellow-500 w-[100%] py-2 px-3 rounded text-white">Edit Data</Link>
                                                )}
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </>
    )
}
export default ShowDataPegawaiComponent