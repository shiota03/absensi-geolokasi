/* eslint-disable react-hooks/rules-of-hooks */
import { Circle, MapContainer, Marker, Popup, TileLayer, useMap, useMapEvents } from "react-leaflet";
import Leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';
import axios from "axios";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { useRouter } from "next/navigation";

const markerIcon = Leaflet.divIcon({
    html: `<svg width="30" height="30" viewBox="0 0 19 25" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M8.41152 24.4956C1.31689 14.2105 0 13.1549 0 9.375C0 4.19731 4.19731 0 9.375 0C14.5527 0 18.75 4.19731 18.75 9.375C18.75 13.1549 17.4331 14.2105 10.3385 24.4956C9.8729 25.1682 8.87705 25.1681 8.41152 24.4956ZM9.375 13.2812C11.5324 13.2812 13.2812 11.5324 13.2812 9.375C13.2812 7.21763 11.5324 5.46875 9.375 5.46875C7.21763 5.46875 5.46875 7.21763 5.46875 9.375C5.46875 11.5324 7.21763 13.2812 9.375 13.2812Z" fill="#FF0202"/>
    </svg>
    `,
    iconSize: [30, 30],
    iconAnchor: [30/2, 30],
    className: 'bg-none'
});

const Maps = () => {
    const router = useRouter();
    const apiKey = process.env.NEXT_PUBLIC_API_KEY;
    const latENV = parseFloat(process.env.NEXT_PUBLIC_LATITUDE || '0');
    const lngENV = parseFloat(process.env.NEXT_PUBLIC_LONGITUDE || '0');

    const [markerPosition, setMarkerPosition] = useState({
        lat: latENV,
        lng: lngENV
    });

    const lokasiKerja = async (api: any) => {
        axios.get(`${api}/lokasiKerja`)
            .then((response) => {
                setMarkerPosition({
                    lat: response.data.data.lat,
                    lng: response.data.data.lng
                });
            })
            .catch((error) => {
                console.error(error);
            });
    };

    useEffect(() => {
        lokasiKerja(apiKey);
    }, [apiKey]);

    const handleMapClick = (e: any) => {
        const { lat, lng } = e.latlng;
        setMarkerPosition({lat:lat, lng:lng});
        console.log(lat, lng)
    };

    const saveLocation = async(e: any) => {
        e.preventDefault();
        const data = {
            lat: markerPosition.lat,
            lng: markerPosition.lng
        }
        Swal.fire({
            title: 'Konfirmasi update lokasi',
            text: "Apakah anda ingin mengperbarui lokasi kantor ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        })
        .then((result) => {
            if (result.isConfirmed) {
                axios.put(`${apiKey}/lokasiKerja`, data)
                .then((response) => {
                    Swal.fire({
                        title: response.data.message,
                        icon: "success"
                    }).then(() => {
                        window.location.reload();
                    })
                })
                .catch((error) => {
                    console.error(error)
                })
            }
        })
    }

    return (
        <div className="lg:col-span-3 md:col-span-4">
            <div className="card rounded-[10px] border border-black p-2 text-center mb-2">
                <legend>
                    <strong>Lokasi Absen</strong>
                </legend>
            </div>
            <hr className="my-2 border border-black border-[.1px]" />
            <div className="card rounded-[10px] border border-black p-2 text-center">
                <MapContainer
                    style={{ height: '300px' }}
                    center={[markerPosition.lat, markerPosition.lng]}
                    zoom={17}
                    scrollWheelZoom={false}
                >
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Circle center={[markerPosition.lat, markerPosition.lng]} pathOptions={{ color: 'blue' }} radius={20}>
                        <Popup>Popup in CircleMarker</Popup>
                    </Circle>
                    <Marker position={[markerPosition.lat, markerPosition.lng]} icon={markerIcon} />
                    <ClickHandler onClick={handleMapClick} />
                </MapContainer>
                <br />
                <button className="bg-blue-700 rounded-[7px] w-[100%] py-2 text-white" onClick={saveLocation}>Simpan</button>
            </div>
        </div>
    );
};

function ClickHandler({ onClick }: { onClick: (e: L.LeafletMouseEvent) => void }) {
    const map = useMap();

    useEffect(() => {
        map.on('click', onClick);
        return () => {
            map.off('click', onClick);
        };
    }, [map, onClick]);

    return null;
}

export default Maps;
