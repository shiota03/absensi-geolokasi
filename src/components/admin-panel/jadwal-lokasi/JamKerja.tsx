import axios from "axios";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";

const JamKerja = () => {

    const apiENV = process.env.NEXT_PUBLIC_API_KEY;

    const [dataJamMasuk, setDataJamMasuk] = useState({
        dari: '',
        sampai: ''
    })
    const [dataJamPulang, setDataJamPulang] = useState('')

    const showJamMasuk = async(api: any) => {
        axios.get(`${api}/jamKerja?status=Masuk`)
        .then((response) => {
            setDataJamMasuk({
                dari: response.data.data.dari,
                sampai: response.data.data.sampai
            })
        })
        .catch((error) => {
            console.error(error)
        })
    }

    const showJamPulang = async(api: any) => {
        axios.get(`${api}/jamKerja?status=Pulang`)
        .then((response) => {
            setDataJamPulang(response.data.data.dari)
        })
        .catch((error) => {
            console.error(error)
        })
    }

    const changeData = (e: any) => {
        const { name, value } = e.target;
        setDataJamMasuk({
            ...dataJamMasuk,
            [name]: value
        })
    }

    const submitFormMasuk = (e: any) => {
        e.preventDefault();
        const dataSend = {
            dari_jam_masuk: dataJamMasuk.dari,
            sampai_jam_masuk: dataJamMasuk.sampai
        };
        axios.put(`${apiENV}/jamKerja?status=Masuk`, dataSend)
        .then((response) => {
            if(response.data.code === 200){
                Swal.fire({
                    icon: 'success',
                    title: response.data.message
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: response.data.message
                })
            }
        })
        .catch((error) => {
            console.error(error)
        })
    }
    const submitFormPulang = (e: any) => {
        e.preventDefault();
        const dataSend = {
            dari_jam_pulang: dataJamPulang
        };
        axios.put(`${apiENV}/jamKerja?status=Pulang`, dataSend)
        .then((response) => {
            if(response.data.code === 200){
                Swal.fire({
                    icon: 'success',
                    title: response.data.message
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: response.data.message
                })
            }
        })
        .catch((error) => {
            console.error(error)
        })
    }

    useEffect(() => {
        showJamMasuk(apiENV);
        showJamPulang(apiENV)
    }, [apiENV])

    return(
        <div className="lg:col-span-2 md:col-span-3">
            <div className="card rounded-[10px] border border-black p-2 text-center mb-2">
                <legend>
                    <strong>Jadwal Kerja</strong>
                </legend>
            </div>
            <hr className="my-2 border border-black border-[.1px]" />
            <div className="card rounded-[10px] border border-black p-2 text-center">
                <legend>
                    <strong>Jam Datang Tepat Waktu Masuk Kerja</strong>
                </legend>
                <hr className="border border-gray-500 my-2" />
                <form onSubmit={submitFormMasuk}>
                    <div>
                        <input 
                            onChange={(e) => {
                                setDataJamMasuk({
                                    ...dataJamMasuk,
                                    dari: e.target.value
                                })
                            }} 
                            defaultValue={dataJamMasuk.dari} 
                            name="dari_masuk" 
                            type="time" 
                            id="dari-jam-masuk" 
                            className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" 
                            required />
                    </div>
                    <h2 className="font-bold my-2">Sampai Dengan</h2>
                    <div>
                        <input 
                            onChange={(e) => {
                                setDataJamMasuk({
                                    ...dataJamMasuk,
                                    sampai: e.target.value
                                })
                            }} 
                            defaultValue={dataJamMasuk.sampai} 
                            name="sampai_masuk" 
                            type="time" 
                            id="sampai-jam-masuk" 
                            className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" 
                            required />
                    </div>
                    <div className="my-2">
                        <button className="bg-blue-700 rounded-[7px] w-[100%] py-2 text-white">Simpan</button>
                    </div>
                </form>
            </div>
            <br />
            <div className="card rounded-[10px] border border-black p-2 text-center">
                <legend>
                    <strong>Jam Boleh Absen Pulang</strong>
                </legend>
                <hr className="border border-gray-500 my-2" />
                <form onSubmit={submitFormPulang}>
                    <div>
                        <input 
                            onChange={(e) => {
                                setDataJamPulang(e.target.value)
                            }} 
                            defaultValue={dataJamPulang} 
                            name="dari_pulang" 
                            type="time" 
                            id="dari-jam-pulang" 
                            className="mt-1 bg-white border border-slate-600 w-[100%] rounded-[7px] py-2 px-2" 
                            required />
                    </div>
                    <div className="my-2">
                        <button className="bg-blue-700 rounded-[7px] w-[100%] py-2 text-white">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    )
}
export default JamKerja;