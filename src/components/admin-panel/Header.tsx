import { useEffect, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import Swal from 'sweetalert2';
import axios from 'axios';
import format from 'date-fns/format';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faUserCircle, faUserCog } from '@fortawesome/free-solid-svg-icons';
import Logout from '../landing-page/auth/Logout';

const HeaderAdminPanel = () => {
    const apiENV = process.env.NEXT_PUBLIC_API_KEY;

    const cekId = () => {
        const userId = localStorage.getItem("user_id");
        const role = localStorage.getItem("user_role");
        if(!userId){
            Swal.fire({
                icon: 'error',
                title: 'Gagal memuat halaman',
                text: 'Silahkan melakukan login terlebih dahulu'
            })
            .then(() => {
                window.location.href = '/login'
            })
        } else if (role === '1') {
            window.location.href = '/user/dashboard'
        }
    }
    
    const [nama, setNama] = useState('');
    const [nomorIdentitas, setNomorIdentitas] = useState('')
    const getUserDataById = async(id: string | null, api: string | undefined) => {
        axios.get(`${api}/loginApi?id=${id}`)
        .then((response) => {
            let userData = response.data.data;
            setNama(userData.nama);
            setNomorIdentitas(userData.nomor_identitas);
        })
        .catch((error) => {
            console.error(error)
        })
    }
    const openHeaderMenu = (e: { preventDefault: () => void; }) => {
        e.preventDefault();
        document.getElementById('menu-header')?.classList.toggle('open')
    }
    const [currentTime, setCurrentTime] = useState(new Date());
    const toggleMenu = () => {
        const sidebar = document.getElementById('sidebar');
        if (sidebar) {
            sidebar.classList.toggle('close');
          }
    }
    const formattedDateTime = format(currentTime, "EEEE, d MMMM y");

    useEffect(() => {
        cekId();
    }, []);
    useEffect(() => {
        const userId = localStorage.getItem("user_id");
        getUserDataById(userId, apiENV);
    }, [apiENV]);
    useEffect(() => {
        const intervalId = setInterval(() => {
            setCurrentTime(new Date());
        }, 1000);
        return () => clearInterval(intervalId);
    }, [])
    
    return(
        <div className='w-[100%] fixed z-[100] top-0'>
            <nav className="bg-white border-gray-200 dark:bg-gray-900">
                <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl p-4">
                    <div className="flex items-center">
                        <FontAwesomeIcon onClick={toggleMenu} icon={faBars} className='bg-slate-200 text-black p-2 rounded-[5px] text-lg cursor-pointer' />
                        <span className="self-center text-2xl ms-2 font-semibold whitespace-nowrap dark:text-white md:block hidden my-0">
                            Sistem Absensi Pegawai
                        </span>
                    </div>
                    <div className="flex items-center">
                        <div className="text-sm text-white duration-200 border rounded py-2 px-5 mr-3 md:mr-0 md:block hidden font-mono">
                            {formattedDateTime}
                        </div>
                        <div>
                            <button onClick={openHeaderMenu} className='text-sm text-white hover:text-blue-200 duration-200 rounded py-2 px-3 ml-3 md:ml-2 md:bg-blue-700 bg-green-900 border border-blue-700'>
                                <FontAwesomeIcon icon={faUserCircle} className='me-1' />
                                {nama}
                            </button>
                        </div>
                    </div>
                </div>
            </nav>
            <div className='relative text-black z-[100]'>
                <div id='menu-header' className='absolute right-8 top-1 text-black bg-gray-900 text-white rounded head-profile'>
                    <ul className='px-3 py-3'>
                        <li className="w-100">
                            <div className="w-100 flex items-center">
                                <Image src='/assets/images/user.png' width={45} height={45} alt="" className="rounded-full me-2" />
                                <div className="text-white">
                                    <h6 className="font-semibold text-[15px]">{ nama }</h6>
                                    <p className="font-medium text-[12px]"><em>{ nomorIdentitas }</em></p>
                                </div>
                            </div> 
                        </li>
                        <li>
                            <hr className='my-2' />
                        </li>
                        <li className="w-100">
                            <Link href="/admin/profil" >
                                <div className="w-100 text-white hover:text-black px-2 py-1 text-[.9rem] flex items-center hover:bg-white duration-300 menu-head">
                                    <div className="text-left w-1/5">
                                        <FontAwesomeIcon icon={faUserCog}  />
                                    </div>
                                    <p className="my-0 font-medium">Profil</p>
                                </div>
                            </Link>
                        </li>
                        <li>
                            <hr className='my-2' />
                        </li>
                        <li className="w-100">
                            <div className="text-white px-2 py-1 text-[.9rem] duration-300 menu-head hover:bg-red-900 duration-300">
                                <Logout />
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}
export default HeaderAdminPanel;