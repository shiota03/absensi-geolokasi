const PageHeaderAdmin = ({ text = '' }) => {
    return(
        <>
            <div className="w-100 bg-slate-300 md:py-3 py-2 md:px-7 px-4 md:rounded-[10px] rounded-[5px] border border-slate-600 mb-3">
                <h2 className="md:text-[18px] text-[12px] font-bold">
                    {text}
                </h2>
            </div>
        </>
    )
}
export default PageHeaderAdmin;