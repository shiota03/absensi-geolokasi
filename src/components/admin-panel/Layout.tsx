import HeaderAdminPanel from "./Header";
import SidebarAdminPanel from "./Sidebar";

const LayoutAdminPanel = () => {
    return(
        <>
            <HeaderAdminPanel />
            <SidebarAdminPanel />
        </>
    )
}
export default LayoutAdminPanel;