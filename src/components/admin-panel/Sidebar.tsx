/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from "react";
import { faDashboard, faList, faMapLocationDot, faMoneyBillTransfer, faUserCog, faUserSecret, faUsers } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import Logout from "../landing-page/auth/Logout";

const SidebarAdminPanel = () => {
    const apiENV = process.env.NEXT_PUBLIC_API_KEY;

    const [nama, setNama] = useState('');
    const [nomorIdentitas, setNomorIdentitas] = useState('');
    const getUserDataById = async(id: string | null, api: string | undefined) => {
        axios.get(`${api}/loginApi?id=${id}`)
        .then((response) => {
            let userData = response.data.data;
            setNama(userData.nama);
            setNomorIdentitas(userData.nomor_identitas);
        })
        .catch((error) => {
            console.error(error)
        })
    }
    useEffect(() => {
        const userId = localStorage.getItem("user_id");
        getUserDataById(userId, apiENV);
    }, [apiENV])

    const logoutProcess = () => {

    }
    const menuSidebar = [
        {icon: faDashboard, menu: 'Dashboard', link: '/admin/dashboard'},
        {icon: faMapLocationDot, menu: 'Jadwal dan Lokasi', link: '/admin/jadwal-lokasi'},
        {icon: faUserSecret, menu: 'Data Admin', link: '/admin/data-admin'},
        {icon: faUsers, menu: 'Data Pegawai', link: '/admin/data-pegawai'},
        {icon: faList, menu: 'Log Absensi Pegawai', link: '/admin/log-absensi-pegawai'},
        {icon: faMoneyBillTransfer, menu: 'Penggajian Pegawai', link: '/admin/penggajian-pegawai'},
        {icon: faUserCog, menu: 'Profil', link: '/admin/profil'}
    ]
    return(
        <>
            <nav id="sidebar" className="fixed h-[100vh] bg-gray-700 top-0 overflow-y-scroll z-10">
                <div className="mt-[80px]">
                    <ul>
                        <li className="w-100">
                            <div className="w-100 flex items-center px-3">
                                <Image src='/assets/images/user.png' width={60} height={60} alt="" className="rounded-full me-2" />
                                <div className="text-white">
                                    <h6 className="font-semibold">{ nama }</h6>
                                    <p className="font-medium"><em>{ nomorIdentitas }</em></p>
                                </div>
                            </div> 
                        </li>
                        <li>
                            <hr className="my-3" />
                        </li>
                        { menuSidebar.map((sideMenu, index) => (
                            <li className="w-100" key={index}>
                                <Link href={sideMenu.link} >
                                    <div className="w-100 text-white hover:text-black px-3 py-2 text-[1.1rem] flex items-center hover:bg-white duration-300 menu-side">
                                        <div className="text-left w-1/6">
                                            <FontAwesomeIcon icon={sideMenu.icon}  />
                                        </div>
                                        <p className="my-0 font-medium">{sideMenu.menu}</p>
                                    </div>
                                </Link>
                            </li>
                        )) }
                        <li>
                            <hr className="my-3" />
                        </li>
                        <li className="w-100">
                            <div className="text-white px-3 py-2 text-[1.1rem] bg-red-900 duration-300 menu-side">
                                <Logout />
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}
export default SidebarAdminPanel;