/* eslint-disable react-hooks/exhaustive-deps */
import { faEdit, faEye, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useQRCode } from "next-qrcode";
import Link from "next/link";
import { useEffect, useState } from "react";

import DataTable from "react-data-table-component";
import Swal from "sweetalert2";

interface UserData {
    id: number;
    nomor_identitas: string;
    nama: string;
    jabatan: string;
  }

const AdminPageDatatable = ({ role = 0 }) => {
    const apiENV = process.env.NEXT_PUBLIC_API_KEY;
    const { SVG } = useQRCode();
   
    const dataColumns = [
        {
            name: 'No',
            selector: (row: { nomor_identitas: any; }, index: any) => index+1
        },
        {
            name: 'Barcode',
            cell: (row: { nomor_identitas: any; }) => (
                <div className="border w-[75%] my-2">
                    <SVG
                        text={row.nomor_identitas}
                        options={{
                            margin: 2,
                            color: {
                                dark: '#000',
                            }
                        }}
                    />
                </div>
            )
        },
        {
            name: 'Nomor Indentitas',
            selector: (row: { nomor_identitas: any; }) => row.nomor_identitas
        },
        {
            name: 'Nama',
            selector: (row: { nama: any; }) => row.nama
        },
        {
            name: 'Jabatan',
            selector: (row: { jabatan: any; }) => row.jabatan
        },
        {
            name: 'Aksi',
            cell: (row: { id: any; }) => (
                <div> 
                    {role > 0 ? (
                        <div className="flex">
                            <div><Link className="bg-blue-500 my-2 mx-1 px-3 py-2 text-white rounded" href={`/admin/data-pegawai/show/${row.id}`}><FontAwesomeIcon icon={faEye} /></Link></div>
                            <div><Link className="bg-yellow-500 my-2 mx-1 px-3 py-2 text-white rounded" href={`/admin/data-pegawai/edit/${row.id}`}><FontAwesomeIcon icon={faEdit} /></Link></div>
                            <div><Link className="bg-red-500 my-2 mx-1 px-3 py-2 text-white rounded" href={'#'} onClick={() => deleteData(row.id)}><FontAwesomeIcon icon={faTrash} /></Link></div>
                        </div>
                    ) : (
                        <div className="flex">
                            <div><Link className="bg-blue-500 my-2 mx-1 px-3 py-2 text-white rounded" href={`/admin/data-admin/show/${row.id}`}><FontAwesomeIcon icon={faEye} /></Link></div>
                            <div><Link className="bg-yellow-500 my-2 mx-1 px-3 py-2 text-white rounded" href={`/admin/data-admin/edit/${row.id}`}><FontAwesomeIcon icon={faEdit} /></Link></div>
                            <div><Link className="bg-red-500 my-2 mx-1 px-3 py-2 text-white rounded" href={'#'} onClick={() => deleteData(row.id)}><FontAwesomeIcon icon={faTrash} /></Link></div>
                        </div>
                    )}
                </div>
            )
        }
    ];

    const [data, setData] = useState<UserData[]>([]);
    const [filter, setFilter] = useState<UserData[]>([]);

    const getProduct = async (api: any) => {
        axios.get(`${api}/showUser?role=${role}`)
        .then((response) => {
            setData(response.data.data)
            setFilter(response.data.data)
        })
        .catch((error) => {
            console.log(error)
        })
    }

    const deleteData = (identitas: any) => {
        const apiENV = process.env.NEXT_PUBLIC_API_KEY;
        Swal.fire({
            title: 'Konfirmasi hapus data',
            text: "Apakah anda ingin menghapus data karyawan ini ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete now'
        })
        .then(( result ) => {
            if (result.isConfirmed) {
                axios.delete(`${apiENV}/showUser?id=${identitas}`)
                .then((response) => {
                    getProduct(apiENV);
                    Swal.fire({
                        title: response.data.message,
                        icon: 'success'
                    })
                })
                .catch((error) => {
                    console.error(error)
                })
            }
        })
        getProduct(apiENV);
    }

    useEffect(() => {
        getProduct(apiENV);
    }, [apiENV])

    const handleSearch = (e: any) => {
        const keyword = e.target.value.toLowerCase();
        const filtered = data.filter((item) => {
          return (
            item.nomor_identitas.toLowerCase().includes(keyword) ||
            item.nama.toLowerCase().includes(keyword) || 
            item.jabatan.toLowerCase().includes(keyword)
          );
        });
        setFilter(filtered);
      };

    return(
        <>
            <DataTable
                className="border"
                columns={dataColumns}
                data={filter}
                highlightOnHover
                pagination
                paginationPerPage={5}
                paginationRowsPerPageOptions={[5, 10, 20, 30]}
                subHeader
                subHeaderComponent={
                    <div className="absolute left-0">
                        <input
                            id="cari"
                            name="cari"
                            type="text"
                            placeholder="Cari data ..."
                            onChange={handleSearch}
                            className="border border-gray-500 rounded py-1 px-2"
                        />
                    </div>
                }
            />
        </>
    )
}
export default AdminPageDatatable;