import { useState, useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';

import format from 'date-fns/format';

const Header = () => {
    const [currentTime, setCurrentTime] = useState(new Date());

    const cekId = () => {
        const userId = localStorage.getItem("user_id");
        const role = localStorage.getItem("user_role");
        if(userId){
            if(role === '0'){
                window.location.href = '/admin/dashboard'
            } else {
                window.location.href = '/user/dashboard'
            }
        }
    }
    useEffect(() => {
        cekId()
        const intervalId = setInterval(() => {
            setCurrentTime(new Date());
        }, 1000);
        return () => clearInterval(intervalId);
    }, []);
    
    // const formattedDateTime = format(currentTime, "EEEE, d MMMM y HH:mm:ss");
    const formattedDateTime = format(currentTime, "EEEE, d MMMM y");

    return (    
        <>
            <nav className="bg-white border-gray-200 dark:bg-gray-900">
                <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl p-4">
                    <Link href="/" className="flex items-center">
                        <Image width={100} height={100} layout="responsive" src="https://flowbite.com/docs/images/logo.svg" className="h-8 mr-3" alt="Flowbite Logo" />
                        <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white md:block hidden">
                            Sistem Absensi Pegawai
                        </span>
                    </Link>
                    <div className="flex items-center">
                        <div className="text-sm text-white duration-200 border rounded py-2 px-5 mr-3 md:mr-0 md:block hidden font-mono">
                            {formattedDateTime}
                        </div>
                        <Link href="/login" className="text-sm text-white hover:text-blue-200 duration-200 rounded py-2 px-5 ml-3 md:ml-2 md:bg-blue-700 bg-green-900 border border-blue-700">Sign In</Link>
                    </div>
                </div>
            </nav>
        </>
    );
};

export default Header;
