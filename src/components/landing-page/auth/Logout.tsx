import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPowerOff } from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";
const Logout = () => {
    const logoutProcess = (e: any) => {
        e.preventDefault();
        Swal.fire({
            title: 'Konfirmasi logout',
            text: "Apakah anda ingin keluar dari halaman ini ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, logout now'
        })
        .then(( result ) => {
            if (result.isConfirmed) {
                Swal.fire({
                    title: 'Berhasil logout!',
                    icon: 'success'
                })
                .then(() => {
                    localStorage.removeItem('user_id')
                    localStorage.removeItem('user_role')
                    window.location.href = '/login'
                })
              }
        })
    }
    return (
        <div className="w-100 flex items-center justify-center cursor-pointer" onClick={logoutProcess}>
            <div className="text-left w-1/6">
                <FontAwesomeIcon icon={faPowerOff}  />
            </div>
            <p className="my-0 font-medium">Logout</p>
        </div>
    );
}
export default Logout;